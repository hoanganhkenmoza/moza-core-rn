import React, {Component} from 'react';
import {
    Text,
    View, TouchableOpacity,
    StyleSheet
} from 'react-native';
import colors from '../../assets/colors';
import {Dimens} from '../../assets/dimens';

export default class ButtonOutline extends Component {

    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={[styles.container,this.props.style]}>
                <Text style={this.props.textStyle}>{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 5,
        padding: 10,
        width: Dimens.screen.width / 2 - 20,
        height: 40,
        borderColor: colors.red,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
});
