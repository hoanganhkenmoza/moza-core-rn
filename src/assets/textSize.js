import {heightPercentageToDP as hp} from '../responsive-screen/index';

export const textSize = {
    mini: hp('1.5%'),
    xmini: hp('1.8%'),
    small: hp('2%'),
    xsmall: hp('2.3%'),
    medium: hp('2.5%'),
    xmedium: hp('2.8%'),
    large: hp('3%'),
    xlarge:hp('3.5%'),
    xslarge: hp('4%'),
    h5: hp('4.5%'),
    h4: hp('5%'),
    h3: hp('5.5%'),
    h2: hp('6%'),
    h1: hp('6.5%'),
};
