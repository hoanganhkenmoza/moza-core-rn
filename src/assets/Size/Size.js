import {heightPercentageToDP as hp, widthPercentageToDP as wp} from '../../responsive-screen';

export const textSize = {
    mini: hp('1.5%'),
    xmini: hp('1.8%'),
    small: hp('2%'),
    xsmall: hp('2.3%'),
    medium: hp('2.5%'),
    xmedium: hp('2.8%'),
    large: hp('3%'),
    xlarge: hp('3.5%'),
    xslarge: hp('4%'),
    h5: hp('4.5%'),
    h4: hp('5%'),
    h3: hp('5.5%'),
    h2: hp('6%'),
    h1: hp('6.5%'),
};
export const row = {
    flexDirection: 'row',
};
export const rowCenter = {
    flexDirection: 'row',
    alignItems: 'center'
};
export const margin10 = {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10
};
export const margin20 = {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginBottom: 20
};
export const horizontalCenter = {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
};
export const horizontal = {
    flexDirection:  'row',
    justifyContent: 'space-between',
};
export const marginHorizontal20 = {
    marginHorizontal: 20
};
export const marginHorizontal10 = {
    marginHorizontal: 10
};
export const marginVertical10 ={
    marginVertical:10
};
export const marginVertical20 ={
    marginVertical:20
};
